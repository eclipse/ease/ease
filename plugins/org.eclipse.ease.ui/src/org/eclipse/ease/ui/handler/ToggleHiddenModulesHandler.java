package org.eclipse.ease.ui.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.State;
import org.eclipse.ease.ui.view.ModuleExplorerView;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.RegistryToggleState;

public class ToggleHiddenModulesHandler extends AbstractHandler {

	private static final String COMMAND_ID = "org.eclipse.ease.commands.ModulesExplorer.toggleHiddenModules";

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		HandlerUtil.toggleCommandState(event.getCommand());

		final IWorkbenchPart part = HandlerUtil.getActivePart(event);
		if (part instanceof ModuleExplorerView)
			((ModuleExplorerView) part).refresh();

		return null;
	}

	public static boolean showHiddenModules() {
		final ICommandService commandService = PlatformUI.getWorkbench().getService(ICommandService.class);

		final Command command = commandService.getCommand(COMMAND_ID);
		final State state = command.getState(RegistryToggleState.STATE_ID);
		return !((boolean) state.getValue());
	}
}
