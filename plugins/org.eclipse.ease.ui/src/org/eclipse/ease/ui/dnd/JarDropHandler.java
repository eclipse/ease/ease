/*******************************************************************************
 * Copyright (c) 2015 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/
package org.eclipse.ease.ui.dnd;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;

import org.eclipse.ease.IScriptEngine;
import org.eclipse.ease.modules.EnvironmentModule;

/**
 * Drop handler accepting jar files. Calls loadJar() on dropped files.
 */
public class JarDropHandler extends AbstractFileDropHandler {

	@Override
	public void performDrop(final IScriptEngine scriptEngine, final Object element) {
		final Method method = getMethod(EnvironmentModule.class, "loadJar", Object.class);
		executeMethod(scriptEngine, method, getFileURI(element));
	}

	@Override
	protected Collection<String> getAcceptedFileExtensions() {
		return Arrays.asList("jar");
	}
}
