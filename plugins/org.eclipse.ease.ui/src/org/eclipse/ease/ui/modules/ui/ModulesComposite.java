/*******************************************************************************
 * Copyright (c) 2014 Bernhard Wedl and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Bernhard Wedl - initial API and implementation
 *******************************************************************************/
package org.eclipse.ease.ui.modules.ui;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.eclipse.core.runtime.IPath;
import org.eclipse.ease.modules.ModuleDefinition;
import org.eclipse.ease.modules.ModuleDefinition.ModuleEntry;
import org.eclipse.ease.ui.tools.DecoratedLabelProvider;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;;

public class ModulesComposite extends Composite {

	private final TreeViewer fTreeViewer;

	/**
	 * Create the composite.
	 *
	 * @param parent
	 * @param style
	 * @param modulesOnly
	 *            if true only the modules are shown in the tree. if false also the fields and functions are shown.
	 */
	public ModulesComposite(final Composite parent, final int style, final boolean modulesOnly) {
		super(parent, style);

		fTreeViewer = new TreeViewer(this, SWT.NONE);

		setLayout(new FillLayout(SWT.HORIZONTAL));
		fTreeViewer.getTree().setLayout(new FillLayout(SWT.HORIZONTAL));

		final DecoratedLabelProvider labelProvider = new DecoratedLabelProvider(new ModulesLabelProvider());
		fTreeViewer.setLabelProvider(labelProvider);

		fTreeViewer.setContentProvider(new ModulesContentProvider(modulesOnly));

		fTreeViewer.setComparator(new ViewerComparator() {
			@Override
			public int category(Object element) {

				// unpack field/method
				if (element instanceof ModuleEntry)
					element = ((ModuleEntry<?>) element).getEntry();

				if ((element instanceof IPath))
					return 1;

				if ((element instanceof ModuleDefinition))
					return 2;

				if ((element instanceof Field))
					return 3;

				if ((element instanceof Method))
					return 4;

				return super.category(element);
			}
		});

		fTreeViewer.addDragSupport(DND.DROP_MOVE | DND.DROP_COPY, new Transfer[] { LocalSelectionTransfer.getTransfer(), TextTransfer.getInstance() },
				new ModulesDragListener(fTreeViewer));

		// input is handled by the content provider directly
		fTreeViewer.setInput("dummy");
	}

	public void setInput(final Object input) {
		fTreeViewer.setInput(input);
	}

	public void refresh() {
		fTreeViewer.refresh();
	}

	public void addFilter(final ViewerFilter filter) {
		fTreeViewer.addFilter(filter);
	}

	public TreeViewer getTreeViewer() {
		return fTreeViewer;
	}
}
