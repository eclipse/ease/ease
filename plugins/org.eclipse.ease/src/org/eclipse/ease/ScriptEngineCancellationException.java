/*******************************************************************************
 * Copyright (c) 2021 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease;

import java.io.PrintStream;
import java.io.PrintWriter;

public class ScriptEngineCancellationException extends RuntimeException {

	private static final long serialVersionUID = 1512188622840964368L;

	public ScriptEngineCancellationException() {
		super("Script terminated on user request");
	}

	@Override
	public String toString() {
		return getMessage();
	}

	@Override
	public void printStackTrace(PrintWriter s) {
		s.println(this);
	}

	@Override
	public void printStackTrace(PrintStream s) {
		s.println(this);
	}
}
