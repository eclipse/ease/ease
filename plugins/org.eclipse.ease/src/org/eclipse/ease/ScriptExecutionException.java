/*******************************************************************************
 * Copyright (c) 2015 Andreas Wallner and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Andreas Wallner - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease;

import java.io.File;
import java.io.PrintStream;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import org.eclipse.core.resources.IFile;
import org.eclipse.ease.debugging.IScriptDebugFrame;
import org.eclipse.ease.debugging.ScriptStackTrace;

/**
 * A common class to be thrown if an error happens during script execution.
 *
 * The individual script engines should convert their internal exceptions into this one, so that we can display nicely formatted and useful error messages if
 * errors happen during script parsing/running.
 */
public class ScriptExecutionException extends ExecutionException {
	private static final long serialVersionUID = 1887518058581732543L;

	private final ScriptStackTrace fScriptStackTrace;

	public ScriptExecutionException(String message, Throwable cause, ScriptStackTrace trace) {
		super(message, cause);

		fScriptStackTrace = Objects.requireNonNullElse(trace, new ScriptStackTrace());
	}

	public ScriptExecutionException(Throwable cause, ScriptStackTrace trace) {
		this(null, cause, trace);
	}

	public ScriptExecutionException(String message, ScriptStackTrace trace) {
		this(message, null, trace);
	}

	public ScriptExecutionException(String message) {
		this(message, null, null);
	}

	public ScriptStackTrace getScriptStackTrace() {
		return fScriptStackTrace;
	}

	@Override
	public void printStackTrace(final PrintStream s) {
		s.println(getMessage());

		for (final IScriptDebugFrame traceElement : fScriptStackTrace) {
			if (traceElement.getScript() != null) {
				String fileName = "<unknown source>";
				final Object file = traceElement.getScript().getFile();
				if (file instanceof IFile)
					fileName = ((IFile) file).getName();
				else if (file instanceof File)
					fileName = ((File) file).getName();

				final String name = ((traceElement.getName() != null) && (!traceElement.getName().isEmpty())) ? ("," + traceElement.getName()) : "";
				final String lineInfo = (traceElement.getLineNumber() > 0) ? ":" + traceElement.getLineNumber() : "";
				s.println("\tat " + fileName + name + lineInfo);
			}
		}

		final Throwable cause = getCause();
		if (cause != null) {
			s.println("\nJava Stacktrace:");
			cause.printStackTrace(s);
		}
	}
}
