/*******************************************************************************
 * Copyright (c) 2023 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease;

import java.util.ArrayList;
import java.util.List;

public class ScriptScheduler {

	private final List<Script> fQueue = new ArrayList<>();
	private boolean fTerminated = false;

	public void addScript(Script script) {
		synchronized (fQueue) {
			if (isTerminated())
				script.setException(new ScriptExecutionException("Engine got terminated"));
			else
				fQueue.add(script);

			fQueue.notifyAll();
		}
	}

	public Script getNextScript() throws InterruptedException {
		synchronized (fQueue) {
			waitForScript();
			return fQueue.remove(0);
		}
	}

	public Script peekNextScript() throws InterruptedException {
		synchronized (fQueue) {
			waitForScript();
			return fQueue.get(0);
		}
	}

	private void waitForScript() throws InterruptedException {
		synchronized (fQueue) {
			while (isEmpty()) {
				if (isTerminated())
					throw new InterruptedException("Scheduler got terminated while waiting for a script");

				fQueue.wait();
			}
		}
	}

	public boolean isEmpty() {
		synchronized (fQueue) {
			return fQueue.isEmpty();
		}
	}

	public void terminate() {
		synchronized (fQueue) {
			fTerminated = true;
			fQueue.stream().forEach(s -> s.setException(new ScriptExecutionException("Engine got terminated")));
			fQueue.clear();

			notifyQueue();
		}
	}

	public boolean isTerminated() {
		return fTerminated;
	}

	public void notifyQueue() {
		synchronized (fQueue) {
			fQueue.notifyAll();
		}
	}

	public void init() {
		fTerminated = false;
	}
}
