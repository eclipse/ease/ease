/*******************************************************************************
 * Copyright (c) 2014 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/
package org.eclipse.ease;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractCodeParser implements ICodeParser {

	public static final Pattern PARAMETER_PATTERN = Pattern.compile("[^\\p{Alnum}-_]*?\\s*([\\p{Alnum}-_]+)\\s*:(.*)");

	public static Map<String, String> extractKeywords(String comment) {
		final Map<String, String> keywords = new HashMap<>();

		if (comment != null) {
			String key = null;
			for (String line : comment.split("\\r?\\n")) {
				final Matcher matcher = PARAMETER_PATTERN.matcher(line);
				if (matcher.matches()) {
					// key value pair found
					key = matcher.group(1);
					keywords.put(key, matcher.group(2).trim());

				} else if (key != null) {
					if (!line.trim().isEmpty()) {
						// check that we do not have a delimiter line (all same chars)
						line = line.trim();
						if (!Pattern.matches("[" + line.charAt(0) + "]+", line))
							// line belongs to previous key value pair
							keywords.put(key, keywords.get(key) + " " + line.trim());
						else
							// line does not belong to previous key anymore
							key = null;
					} else
						// remove cached key as we hit an empty line
						key = null;
				}

				// any other line will be ignored
			}
		}

		return keywords;
	}

	/**
	 * Default implementation to extract the first comment area from a stream. Looks for block and line comments. Might be replaced by more specific
	 * implementations for dedicated languages.
	 *
	 * @param stream
	 *            code content stream
	 * @return comment data without decoration characters (eg: '*' at beginning of each line)
	 */
	@Override
	public String getHeaderComment(final InputStream stream) {
		final StringBuilder comment = new StringBuilder();

		final BufferedReader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8));

		boolean isComment = true;
		boolean isBlock = false;
		try {
			do {
				String line = reader.readLine();
				if (line == null)
					break;

				line = line.trim();

				if (isBlock) {
					if (line.contains(getBlockCommentEndToken())) {
						isBlock = false;
						line = line.substring(0, line.indexOf(getBlockCommentEndToken()));
					}

					comment.append(stripCommentLine(line.trim())).append('\n');
					continue;

				} else if ((hasBlockComment()) && (line.startsWith(getBlockCommentStartToken()))) {
					isBlock = true;
					line = line.substring(getBlockCommentStartToken().length()).trim();
					comment.append(stripCommentLine(line)).append('\n');
					continue;

				} else if (line.startsWith(getLineCommentToken())) {
					comment.append(stripCommentLine(line.substring(getLineCommentToken().length()).trim())).append('\n');
					continue;
				}

				if (line.isEmpty())
					continue;

				// not a comment line, not empty
				if (!isAcceptedBeforeHeader(line))
					isComment = false;

			} while (isComment);

		} catch (final IOException e) {
			Logger.error(Activator.PLUGIN_ID, "Could not parse input stream header", e);
			return "";
		}

		return comment.toString();
	}

	/**
	 * Allows to remove special delimiter characters from a comment line. Typically comments might start with a character like '*' or '#' depending on the
	 * script language.
	 *
	 * @param commentLine
	 *            single comment line
	 * @return modified comment line
	 */
	protected String stripCommentLine(String commentLine) {
		return commentLine;
	}

	@Override
	public boolean isAcceptedBeforeHeader(String line) {
		return false;
	}

	protected abstract boolean hasBlockComment();

	protected abstract String getBlockCommentEndToken();

	protected abstract String getBlockCommentStartToken();

	protected abstract String getLineCommentToken();
}
