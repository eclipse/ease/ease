/*******************************************************************************
 * Copyright (c) 2023 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease.lang.unittest.ui.variablesprovider;

import java.util.List;

/**
 * How to use this provider: Open *.suite file in a text editor and set the variable definition to <code>
 *   &lt;variable fullName="/TRUE_OR_FALSE" enumProviderID="org.eclipse.ease.lang.unittest.ui.booleanProvider" /&gt;
 * </code>.
 */
public class BooleanProvider extends ConstantValueProvider {

	public BooleanProvider() {
		setValues(List.of("true", "false"));
	}
}
