package org.eclipse.ease.lang.unittest.ui.variablesprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.ease.lang.unittest.ui.editor.IVariablesProvider;

/**
 * How to use this provider: Open *.suite file in a text editor and set the variable definition to <code>
 *   &lt;variable fullName="/SOME_CONSTANT" enumProviderID="org.eclipse.ease.lang.unittest.ui.constantValueProvider#Option A|Option B" /&gt;
 * </code>.
 */
public class ConstantValueProvider implements IVariablesProvider {

	private List<String> fValues = Collections.emptyList();

	@Override
	public boolean allowsCustomValues() {
		return false;
	}

	public void setValues(List<String> values) {
		fValues = new ArrayList<>(values);
	}

	@Override
	public List<String> getValues() {
		return Collections.unmodifiableList(fValues);
	}
}
