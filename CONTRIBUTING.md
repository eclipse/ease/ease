# Contributing to Eclipse Advanced Scripting Environment (EASE)

Thanks for your interest in this project.


## Project description:

EASE allows the execution of script code (using scripting languages such as JS, Python, ...) within the context of the Eclipse Platform/RCP. As they are running in the same JRE as Eclipse itself, these scripts have direct access to any class of your application and may interact with your workbench.

Through keywords within header data, scripts might be integrated seamlessly into the UI as toolbar and/or menu contributions. Thus the functionality could be extended by providing scripts only. No need to build, export and install a new feature, just provide a script and add it to your application at runtime.

Scripting allows to easily extend an existing application, helps in automating everyday tasks and aids in rapid prototyping during application development. 


## Developer resources:

Information regarding source code management, builds, coding standards, and more.

* [https://projects.eclipse.org/projects/technology.ease/developer](https://projects.eclipse.org/projects/technology.ease/developer)


## Source Code Repositories:

The project maintains its source code repositories on gitlab:

[https://gitlab.eclipse.org/eclipse/ease](https://gitlab.eclipse.org/eclipse/ease)

* Ease

  Contains the source code for plugins & features

* Ease Scripts

  Sample scripts and demos that users can consume

* Ease Website

  Website generation code  

## Contributor Agreement:

Before your contribution can be accepted by the project, you need to create and electronically sign the Eclipse Contributor Agreement (ECA).

* [https://www.eclipse.org/legal/ECA.php](https://www.eclipse.org/legal/ECA.php)


## Contact:

Contact the project developers via the project's "dev" list.

* [https://dev.eclipse.org/mailman/listinfo/ease-dev](https://dev.eclipse.org/mailman/listinfo/ease-dev)


## Bugtracking:

This project uses [Gitlab issues](https://gitlab.eclipse.org/groups/eclipse/ease/-/issues) to track ongoing development and issues.

Follow [this link](https://gitlab.eclipse.org/eclipse/ease/ease/-/issues/new) to enter new bugs.

Be sure to search for existing bugs before you create another one. Remember that contributions are always welcome!
