EASE
====

This is the source code repository of Eclipse's EASE project. It contains the main components of scripting support along with provided script engine implementations and some generic scripting libraries to be used with EASE.

For more information and important links, refer to the [EASE documentation page] [1] or the [EASE project overview page] [2].

License
-------

[Eclipse Public License (EPL) v2.0][3]

[1]: https://www.eclipse.org/ease/documentation/
[2]: https://www.eclipse.org/ease/
[3]: https://wiki.eclipse.org/EPL
