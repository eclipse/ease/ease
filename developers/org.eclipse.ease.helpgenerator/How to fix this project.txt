This project builds the javadoc doclet for module documentation generation.
It is an add-on to EASE typically used in automated builds.

To use it locally you need to use an Oracle Java 9 JDK and register it in: Preferences/Java/Installed JREs

In case this project is broken in your workspace you may safely close the project.

A current build of the doclet can directly be downloaded from:
	https://www.eclipse.org/ease/download/