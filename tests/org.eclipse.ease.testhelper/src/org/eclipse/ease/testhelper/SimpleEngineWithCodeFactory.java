/*******************************************************************************
 * Copyright (c) 2023 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease.testhelper;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.eclipse.ease.ICodeFactory;
import org.eclipse.ease.IScriptEngine;
import org.eclipse.ease.ScriptResult;
import org.eclipse.ease.modules.IEnvironment;
import org.eclipse.ease.service.EngineDescription;
import org.eclipse.ease.service.ScriptType;

public final class SimpleEngineWithCodeFactory {

	@Deprecated
	private SimpleEngineWithCodeFactory() {
		throw new UnsupportedOperationException(
				String.format("%s is a helper class and shall not be instantiated", SimpleEngineWithCodeFactory.class.getSimpleName()));
	}

	public static IScriptEngine create() {
		final IEnvironment environment = mock(IEnvironment.class);

		final ScriptType scriptType = mock(ScriptType.class);
		when(scriptType.getCodeFactory()).thenReturn(new SimpleCodeFactory());

		final EngineDescription engineDescription = mock(EngineDescription.class);
		when(engineDescription.getSupportedScriptTypes()).thenReturn(List.of(scriptType));

		final IScriptEngine scriptEngine = mock(IScriptEngine.class);
		when(scriptEngine.getDescription()).thenReturn(engineDescription);
		when(scriptEngine.getVariables()).thenReturn(Map.of("", environment));
		when(scriptEngine.execute(anyString())).thenAnswer(a -> {
			final ScriptResult result = new ScriptResult();
			result.setResult(a.getArguments()[0]);
			return result;
		});

		return scriptEngine;
	}

	private static class SimpleCodeFactory implements ICodeFactory {

		@Override
		public String getSaveVariableName(String variableName) {
			return variableName;
		}

		@Override
		public String classInstantiation(Class<?> clazz, String[] parameters) {
			final String params = Arrays.asList(parameters).stream().map(Objects::toString).collect(Collectors.joining(", "));
			return String.format("new %s(%s)", clazz.getName(), params);
		}

		@Override
		public String createFunctionCall(Method method, Object... parameters) {
			final String params = Arrays.asList(parameters).stream().map(Objects::toString).collect(Collectors.joining(", "));
			return String.format("%s(%s)", method.getName(), params);
		}

		@Override
		public String getDefaultValue(Parameter parameter) {
			return parameter.getDefaultValue();
		}

		@Override
		public String createCommentedString(String comment, boolean blockComment) {
			return String.format("// %s", comment);
		}

		@Override
		public String createWrapper(IEnvironment environment, Object instance, String identifier, boolean customNamespace, IScriptEngine engine) {
			return null;
		}
	}
}
