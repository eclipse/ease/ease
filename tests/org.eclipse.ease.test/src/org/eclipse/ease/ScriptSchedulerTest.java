/*******************************************************************************
 * Copyright (c) 2023 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ScriptSchedulerTest {

	private ScriptScheduler fScheduler;

	@BeforeEach
	public void beforeEach() {
		fScheduler = new ScriptScheduler();
	}

	@Test
	@DisplayName("isEmpty() = true ... by default")
	public void isEmpty_is_true_by_default() {
		assertTrue(fScheduler.isEmpty());
	}

	@Test
	@DisplayName("isEmpty() = false ... after adding a script")
	public void isEmpty_is_false_after_adding_a_script() {
		fScheduler.addScript(new Script(""));
		assertFalse(fScheduler.isEmpty());
	}

	@Test
	@DisplayName("isEmpty() = true ... after adding and removing a script")
	public void isEmpty_is_true_after_adding_and_removing_a_script() throws InterruptedException {
		fScheduler.addScript(new Script(""));
		fScheduler.getNextScript();

		assertTrue(fScheduler.isEmpty());
	}

	@Test
	@DisplayName("getNextScript() returns added script")
	public void getNextScript_returns_added_script() throws InterruptedException {
		final Script script = new Script("");
		fScheduler.addScript(script);
		assertEquals(script, fScheduler.getNextScript());
	}

	@Test
	@DisplayName("getNextScript() returns scripts in right order")
	public void getNextScript_returns_scripts_in_right_order() throws InterruptedException {
		final Script script1 = new Script("1");
		final Script script2 = new Script("2");
		fScheduler.addScript(script1);
		fScheduler.addScript(script2);

		assertEquals(script1, fScheduler.getNextScript());
		assertEquals(script2, fScheduler.getNextScript());
	}

	@Test
	@DisplayName("peekNextScript() returns added script")
	public void peekNextScript_returns_added_script() throws InterruptedException {
		final Script script = new Script("");
		fScheduler.addScript(script);
		assertEquals(script, fScheduler.peekNextScript());
	}

	@Test
	@DisplayName("peekNextScript() does not remove script")
	public void peekNextScript_does_not_remove_script() throws InterruptedException {
		final Script script = new Script("");
		fScheduler.addScript(script);

		assertEquals(script, fScheduler.peekNextScript());
		assertEquals(script, fScheduler.peekNextScript());
	}

	@Test
	@DisplayName("getNextScript() throws ... when scheduler gets terminated")
	public void getNextScript_throws_when_scheduler_gets_terminated() {
		new Job("Inject script") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				fScheduler.terminate();

				return Status.OK_STATUS;
			}
		}.schedule(500);

		assertThrows(InterruptedException.class, () -> fScheduler.getNextScript());
	}

	@Test
	@DisplayName("terminate() sets exceptions on pending scripts")
	public void shutdown_sets_exceptions_on_pending_scripts() {
		final Script script1 = new Script("1");
		final Script script2 = new Script("2");
		fScheduler.addScript(script1);
		fScheduler.addScript(script2);

		fScheduler.terminate();

		assertThrows(ScriptExecutionException.class, () -> script1.getResult().get());
		assertThrows(ScriptExecutionException.class, () -> script2.getResult().get());
	}

	@Test
	@DisplayName("addScript() sets script exception ... after shutdown()")
	public void addScript_sets_script_exception_after_shutdown() {
		fScheduler.terminate();

		final Script script = new Script("");
		fScheduler.addScript(script);

		assertThrows(ScriptExecutionException.class, () -> script.getResult().get());
	}

	@Test
	@DisplayName("addScript() activates waiting thread")
	public void addScript_activates_waiting_thread() throws InterruptedException {
		final Script script = new Script("");

		new Job("Inject script") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				fScheduler.addScript(script);

				return Status.OK_STATUS;
			}
		}.schedule(500);

		assertEquals(script, fScheduler.getNextScript());
	}

	@Test
	@DisplayName("isTerminated() = false ... by default")
	public void isTerminated_is_false_by_default() {
		assertFalse(fScheduler.isTerminated());
	}

	@Test
	@DisplayName("isTerminated() = true ... after shutdown")
	public void isTerminated_is_true_after_shutdown() {
		fScheduler.terminate();
		assertTrue(fScheduler.isTerminated());
	}

	@Test
	@DisplayName("init() resets termination status")
	public void init_reset_termination_status() {
		fScheduler.terminate();
		fScheduler.init();
		assertFalse(fScheduler.isTerminated());
	}
}
