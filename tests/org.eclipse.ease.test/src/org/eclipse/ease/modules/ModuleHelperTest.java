/*******************************************************************************
 * Copyright (c) 2023 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease.modules;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ModuleHelperTest {

	@Test
	@DisplayName("instanceToDefinition() finds module definition")
	public void instanceToDefinition_finds_module_definition() {

		final ModuleDefinition definition = ModuleHelper.instanceToDefinition(new EnvironmentModule());
		assertNotNull(definition);
		assertEquals("org.eclipse.ease.environment", definition.getId());
	}

	@Test
	@DisplayName("instanceToDefinition() = null ... for unknown instance")
	public void instanceToDefinition_is_null_for_unknown_instance() {

		final ModuleDefinition definition = ModuleHelper.instanceToDefinition("unknown");
		assertNull(definition);
	}
}
