/*******************************************************************************
 * Copyright (c) 2021 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease.modules;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import org.eclipse.ease.modules.ModuleDefinition.ModuleEntry;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class ModuleDefinitionTest {

	public static final String TEST_FIELD = "";

	@Test
	@DisplayName("forInstance(module) returns definition")
	public void forInstance_returns_definition() {
		final ModuleDefinition definition = ModuleDefinition.forInstance(new EnvironmentModule());

		assertNotNull(definition);
		assertEquals("Environment", definition.getName());
	}

	@Test
	@DisplayName("forInstance(unknown) = null")
	public void forInstance_returns_null() {
		assertNull(ModuleDefinition.forInstance("foo"));
	}

	@Test
	@DisplayName("getModuleEntries() detects fields")
	public void getModuleEntries_detects_fields() {
		final ModuleDefinition definition = ModuleDefinition.forInstance(new TestModule());

		final List<ModuleEntry<?>> entries = definition.getModuleEntries();
		assertEquals("TEST_FIELD", entries.get(0).getName());
	}

	@Test
	@DisplayName("getModuleEntries() detects methods")
	public void getModuleEntries_detects_methods() {
		final ModuleDefinition definition = ModuleDefinition.forInstance(new TestModule());

		final List<ModuleEntry<?>> entries = definition.getModuleEntries();
		assertEquals("testMethod", entries.get(1).getName());
	}

	@ParameterizedTest(name = "for '{0}'")
	@ValueSource(strings = { "TEST_FIELD", "testMethod" })
	@DisplayName("containsEntry() = true")
	public void containsEntry_is_true(String entry) {
		final ModuleDefinition definition = ModuleDefinition.forInstance(new TestModule());

		assertTrue(definition.containsEntry(entry));
	}

	@Test
	@DisplayName("containsEntry() = false")
	public void containsEntry_is_false() {
		final ModuleDefinition definition = ModuleDefinition.forInstance(new TestModule());

		assertFalse(definition.containsEntry("not there"));
	}

	@Test
	@DisplayName("ModuleEntry.getModuleDefinition() returns parent definition")
	public void ModuleEntry_getModuleDefinition_returns_parnet_Definition() {
		final ModuleDefinition definition = mock(ModuleDefinition.class);

		assertEquals(definition, new ModuleEntry<>(definition, null).getModuleDefinition());
	}

	@Test
	@DisplayName("ModuleEntry.getEntry() returns entry")
	public void ModuleEntry_getEntry_returns_entry() {

		final Method method = ModuleDefinitionTest.class.getMethods()[0];
		assertEquals(method, new ModuleEntry<>(null, method).getEntry());
	}

	@Test
	@DisplayName("ModuleEntry.getName() returns method name")
	public void ModuleEntry_getName_returns_method_name() {
		final Method method = ModuleDefinitionTest.class.getMethods()[0];
		assertEquals(method.getName(), new ModuleEntry<>(null, method).getName());
	}

	@Test
	@DisplayName("ModuleEntry.getName() returns field name")
	public void ModuleEntry_getName_returns_field_name() {
		final Field field = ModuleDefinitionTest.class.getFields()[0];
		assertEquals(field.getName(), new ModuleEntry<>(null, field).getName());
	}

	@Test
	@DisplayName("ModuleEntry.getName() returns generic String")
	public void ModuleEntry_getName_returns_generic_String() {
		assertEquals("foo", new ModuleEntry<>(null, "foo").getName());
	}
}
