/*******************************************************************************
 * Copyright (c) 2023 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ScriptEngineCancellationExceptionTest {

	@Test
	@DisplayName("toString() does not throw")
	public void toString_does_not_throw() {
		assertDoesNotThrow(() -> new ScriptEngineCancellationException().toString());
	}
}
