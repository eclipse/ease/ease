/*******************************************************************************
 * Copyright (c) 2023 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease.lang.unittest.ui.variablesprovider;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class BooleanProviderTest {

	@Test
	@DisplayName("allowsCustomValues() = false")
	public void allowsCustomValues_is_false() {
		assertFalse(new BooleanProvider().allowsCustomValues());
	}

	@Test
	@DisplayName("getValues() = [true, false]")
	public void getValues_contains_true_false() {
		final List<String> values = new BooleanProvider().getValues();

		assertEquals(2, values.size());
		assertTrue(values.contains("true"));
		assertTrue(values.contains("false"));
	}
}
