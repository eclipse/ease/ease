/*******************************************************************************
 * Copyright (c) 2023 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease.lang.unittest.ui.variablesprovider;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ConstantValueProviderTest {

	@Test
	@DisplayName("allowsCustomValues() = false")
	public void allowsCustomValues_is_false() {
		assertFalse(new ConstantValueProvider().allowsCustomValues());
	}

	@Test
	@DisplayName("getValues() = [] ... by default")
	public void getValues_is_empty_by_default() {
		assertTrue(new ConstantValueProvider().getValues().isEmpty());
	}

	@Test
	@DisplayName("getValues() contains set values")
	public void getValues_contains_set_values() {
		final ConstantValueProvider provider = new ConstantValueProvider();
		provider.setValues(List.of("A", "B", "C"));

		assertArrayEquals(new String[] { "A", "B", "C" }, provider.getValues().toArray(new String[0]));
	}
}
