/*******************************************************************************
 * Copyright (c) 2023 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease.ui.modules.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.eclipse.ease.modules.ModuleDefinition;
import org.eclipse.ease.modules.ModuleDefinition.ModuleEntry;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ModulesContentProviderTest {

	@Test
	@DisplayName("getParent() = null")
	public void getParent_is_null() {
		assertNull(new ModulesContentProvider(false).getParent(null));
	}

	@Test
	@DisplayName("getChildren() returns module entries")
	public void getChildren_returns_module_entries() {
		final ModuleDefinition moduleDefinition = mock(ModuleDefinition.class);
		when(moduleDefinition.getModuleEntries()).thenReturn(List.of(new ModuleEntry<>(null, null)));

		final ModulesContentProvider provider = new ModulesContentProvider(false);

		assertEquals(1, provider.getChildren(moduleDefinition).length);
	}

	@Test
	@DisplayName("getChildren() = empty ... when modulesOnly flag is set")
	public void getChildren_is_empty_when_modulesonly_flag_is_set() {
		final ModuleDefinition moduleDefinition = mock(ModuleDefinition.class);
		when(moduleDefinition.getModuleEntries()).thenReturn(List.of(new ModuleEntry<>(null, null)));

		final ModulesContentProvider provider = new ModulesContentProvider(true);

		assertEquals(0, provider.getChildren(provider).length);
	}

	@Test
	@DisplayName("getChildren() = empty ... when parent is unknown")
	public void getChildren_is_empty_when_parent_is_unknown() {
		final ModulesContentProvider provider = new ModulesContentProvider(false);

		assertEquals(0, provider.getChildren("unknown").length);
	}

	@Test
	@DisplayName("hasChildren() = true ... for module definition")
	public void hasChildren_is_true_for_module_definition() {
		final ModuleDefinition moduleDefinition = mock(ModuleDefinition.class);
		when(moduleDefinition.getModuleEntries()).thenReturn(List.of(new ModuleEntry<>(null, null)));

		final ModulesContentProvider provider = new ModulesContentProvider(false);

		assertTrue(provider.hasChildren(moduleDefinition));
	}

	@Test
	@DisplayName("hasChildren() = false ... when modulesOnly flag is set")
	public void hasChildren_is_false_when_modulesonly_flag_is_set() {
		final ModuleDefinition moduleDefinition = mock(ModuleDefinition.class);
		when(moduleDefinition.getModuleEntries()).thenReturn(List.of(new ModuleEntry<>(null, null)));

		final ModulesContentProvider provider = new ModulesContentProvider(true);

		assertFalse(provider.hasChildren(moduleDefinition));
	}

	@Test
	@DisplayName("hasChildren() = false ... when parent is unknown")
	public void hasChildren_is_false_when_parent_is_unknown() {
		final ModulesContentProvider provider = new ModulesContentProvider(false);

		assertFalse(provider.hasChildren("unknown"));
	}
}
