/*******************************************************************************
 * Copyright (c) 2023 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease.ui.dnd;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;

import java.util.concurrent.ExecutionException;

import org.eclipse.ease.IScriptEngine;
import org.eclipse.ease.testhelper.SimpleEngineWithCodeFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

public class JarDropHandlerTest {

	@Test
	@DisplayName("performDrop() calls loadJar()")
	public void performDrop_calls_loadJar() throws ExecutionException {
		final IScriptEngine engine = SimpleEngineWithCodeFactory.create();

		new JarDropHandler().performDrop(engine, "/path/to/archive.jar");

		final ArgumentCaptor<Object> codeCaptor = ArgumentCaptor.forClass(Object.class);

		verify(engine).execute(codeCaptor.capture());
		assertEquals("loadJar(file:///path/to/archive.jar)", codeCaptor.getValue());
	}

	@Test
	@DisplayName("getAcceptedFileExtensions() contains 'jar'")
	public void getAcceptedFileExtensions_contains_jar() {
		assertTrue(new JarDropHandler().getAcceptedFileExtensions().contains("jar"));
	}
}
