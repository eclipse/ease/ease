/*******************************************************************************
 * Copyright (c) 2023 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease.ui.dnd;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.eclipse.ease.ICodeFactory;
import org.eclipse.ease.IScriptEngine;
import org.eclipse.ease.modules.EnvironmentModule;
import org.eclipse.ease.modules.IEnvironment;
import org.eclipse.ease.service.EngineDescription;
import org.eclipse.ease.service.ScriptType;
import org.eclipse.ease.testhelper.SimpleEngineWithCodeFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

public class AbstractModuleDropHandlerTest {

	private AbstractModuleDropHandler fHandler;

	@BeforeEach
	public void beforeEach() {
		fHandler = spy(AbstractModuleDropHandler.class);
	}

	@Test
	@DisplayName("getMethod() returns existing method")
	public void getMethod_returns_existing_method() {
		assertNotNull(fHandler.getMethod(String.class, "toString"));
	}

	@Test
	@DisplayName("getMethod() returns existing method_with_parameters")
	public void getMethod_returns_existing_method_with_parameters() {
		assertNotNull(fHandler.getMethod(String.class, "substring", int.class, int.class));
	}

	@Test
	@DisplayName("getMethod() = null ... when method does not exist")
	public void getMethod_is_null_when_method_does_not_exist() {
		assertNull(fHandler.getMethod(String.class, "notThere", Object.class));
	}

	@Test
	@DisplayName("getMethod() = null ... when method parameters are wrong")
	public void getMethod_is_null_when_method_parameters_ARE_WRONG() {
		assertNull(fHandler.getMethod(String.class, "substring", int.class, double.class));
	}

	@Test
	@DisplayName("getMethod() = null ... when method is not accessible")
	public void getMethod_is_null_when_method_is_not_accessible() {
		assertNull(fHandler.getMethod(AbstractModuleDropHandler.class, "getLoadedModule", IScriptEngine.class, String.class));
	}

	@Test
	@DisplayName("executeMethod() executes method call")
	public void executeMethod_executes_method_call() {
		final ICodeFactory codeFactory = mock(ICodeFactory.class);

		final ScriptType scriptType = mock(ScriptType.class);
		when(scriptType.getCodeFactory()).thenReturn(codeFactory);

		final EngineDescription engineDescription = mock(EngineDescription.class);
		when(engineDescription.getSupportedScriptTypes()).thenReturn(List.of(scriptType));

		final IScriptEngine scriptEngine = mock(IScriptEngine.class);
		when(scriptEngine.getDescription()).thenReturn(engineDescription);

		final Method method = fHandler.getMethod(String.class, "substring", int.class, int.class);

		fHandler.executeMethod(scriptEngine, method, 2, 5);
		verify(scriptEngine).execute(any());
	}

	@Test
	@DisplayName("executeMethod() throws ... when method = null")
	public void executeMethod_throws_when_method_is_null() {
		assertThrows(IllegalArgumentException.class, () -> fHandler.executeMethod(null, null, 1));
	}

	@Test
	@DisplayName("loadModule(force=false) loads unloaded module")
	public void loadModule_force_is_false_loads_unloaded_module() throws ExecutionException {
		final IScriptEngine scriptEngine = SimpleEngineWithCodeFactory.create();

		fHandler.loadModule(scriptEngine, EnvironmentModule.MODULE_NAME, false);

		final ArgumentCaptor<Object> codeCaptor = ArgumentCaptor.forClass(Object.class);

		verify(scriptEngine).execute(codeCaptor.capture());
		assertEquals("loadModule(/System/Environment, false)", codeCaptor.getValue());
	}

	@Test
	@DisplayName("loadModule(force=true) reloads loaded module")
	public void loadModule_force_is_true_reloads_loaded_module() throws ExecutionException {
		final IScriptEngine scriptEngine = SimpleEngineWithCodeFactory.create();

		final IEnvironment environment = IEnvironment.getEnvironment(scriptEngine);
		when(environment.getModule(anyString())).thenReturn("");

		fHandler.loadModule(scriptEngine, EnvironmentModule.MODULE_NAME, true);

		final ArgumentCaptor<Object> codeCaptor = ArgumentCaptor.forClass(Object.class);

		verify(scriptEngine).execute(codeCaptor.capture());
		assertEquals("loadModule(/System/Environment, false)", codeCaptor.getValue());
	}

	@Test
	@DisplayName("loadModule(force=false) does not load loaded module")
	public void loadModule_force_is_false_does_not_load_loaded_module() throws ExecutionException {
		final IScriptEngine scriptEngine = SimpleEngineWithCodeFactory.create();

		final IEnvironment environment = IEnvironment.getEnvironment(scriptEngine);
		when(environment.getModule(anyString())).thenReturn("");

		fHandler.loadModule(scriptEngine, EnvironmentModule.MODULE_NAME, false);

		verify(scriptEngine, never()).execute(any());
	}

	@Test
	@DisplayName("loadModule() returns module instance")
	public void loadModule_returns_module_instance() throws ExecutionException {
		final IScriptEngine scriptEngine = SimpleEngineWithCodeFactory.create();

		assertNotNull(fHandler.loadModule(scriptEngine, EnvironmentModule.MODULE_NAME, false));
	}

	@Test
	@DisplayName("loadModule() = null ... for unknown module")
	public void loadModule_is_null_for_unknown_module() throws ExecutionException {
		final IEnvironment environment = mock(IEnvironment.class);
		when(environment.loadModule(anyString(), anyBoolean())).thenReturn(new Object());

		final IScriptEngine scriptEngine = mock(IScriptEngine.class);
		when(scriptEngine.getVariables()).thenReturn(Map.of("", environment));

		assertNull(fHandler.loadModule(scriptEngine, "does not exist", false));
	}
}
