/*******************************************************************************
 * Copyright (c) 2023 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease.modules.platform;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import java.io.FileNotFoundException;

import org.eclipse.ease.IScriptEngine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class UIModuleTest {

	private UIModule fModule;

	@BeforeEach
	public void beforeEach() {
		fModule = new UIModule();

		final IScriptEngine engine = mock(IScriptEngine.class);
		fModule.initialize(engine, null);
	}

	@Test
	@DisplayName("openInSystemEditor() throws ... when file does not exist")
	public void openInSystemEditor_throws_when_file_does_not_exist() {
		assertThrows(FileNotFoundException.class, () -> fModule.openInSystemEditor("file://does_not_exist"));
	}

	@Test
	@DisplayName("openInSystemEditor() throws ... when input is null")
	public void openInSystemEditor_throws_when_input_is_null() {
		assertThrows(FileNotFoundException.class, () -> fModule.openInSystemEditor(null));
	}
}
