/*******************************************************************************
 * Copyright (c) 2015 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License_Identifier: EPL-2.0
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

package org.eclipse.ease.modules.platform;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTimeout;

import java.io.IOException;
import java.time.Duration;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.osgi.service.prefs.BackingStoreException;

public class PlatformModuleTest {

	@Test
	@DisplayName("getSystemProperty() returns existing property")
	public void getSystemProperty_returns_existing_property() {
		assertNotNull(new PlatformModule().getSystemProperty("java.home"));
	}

	@Test
	@DisplayName("getSystemProperty() = null ... when property does not exist")
	public void getSystemProperty_is_null_wehen_property_does_not_exist() {
		assertNull(new PlatformModule().getSystemProperty("java.home.undefined"));
	}

	@Test
	@DisplayName("runProcess() terminates")
	public void runProcess() throws IOException, InterruptedException {
		final Process process = new PlatformModule().runProcess("ls", new String[] { "-la" }, "keep", null);

		assertTimeout(Duration.ofSeconds(3), () -> process.waitFor());
	}

	@Test
	@DisplayName("writePreferences() stores preferences")
	public void writePreferences_stores_preferences() throws BackingStoreException {
		final String node = "org.eclipse.ease.modules.platform";
		new PlatformModule().writePreferences(node, "key", "value");

		assertEquals("value", InstanceScope.INSTANCE.getNode(node).get("key", ""));
	}
}
