pipeline {

	agent {
	    label 'migration'	    
	}

    options {
        timestamps()
        timeout(time: 60, unit: 'MINUTES')
    }

    environment {
        JAVA_VERSION = "openjdk-jdk17-latest"
        MAVEN_VERSION = "apache-maven-latest"
    }
              
    stages {

        stage('Build Doclet') {
			steps {
				withMaven(
					jdk: 'oracle-jdk9-latest',
					maven: 'apache-maven-latest',
					options: [artifactsPublisher(disabled: true), findbugsPublisher(), openTasksPublisher(disabled: true)])
				{
					sh 'mvn clean verify -f developers/org.eclipse.ease.helpgenerator/pom.xml'
				}
				
				sh 'cp developers/org.eclipse.ease.helpgenerator/target/ease.module.doclet.jar .'
			}
        }

        stage('Build EASE') {
            steps {
	            withMaven(
	            	jdk: "${JAVA_VERSION}",
	            	maven: "${MAVEN_VERSION}",
	            	options: [
	            	    artifactsPublisher(disabled: true),
	            	    junitPublisher(disabled: true),
	            	    findbugsPublisher(disabled: true),
	            	    jgivenPublisher(disabled: true),
	            	    jacocoPublisher(disabled: true),
	            	    openTasksPublisher(disabled: true),
	            	    spotbugsPublisher(disabled: true)
	            	])
	            {
					sh """
					    export JENKINS_MAVEN_AGENT_DISABLED=true
						mvn clean verify -Pmodule-docs -Psign
					"""
                }
            }
        }
        
        stage('Reporting') {
            steps {
    			junit '**/target/surefire-reports/*.xml'
                recordIssues aggregatingResults: true, tools: [mavenConsole(), cpd(), pmdParser(), checkStyle(), spotBugs(useRankAsPriority: true)]
                publishCoverage adapters: [jacocoAdapter(path: 'releng/org.eclipse.ease.releng.coverage/target/site/jacoco-aggregate/jacoco.xml')], sourceFileResolver: sourceFiles('STORE_LAST_BUILD')
            }
        }        
    }
    
    post {
        failure {
			archiveArtifacts artifacts: '**', fingerprint: false
            emailext body: """
Build ${JOB_NAME} failed.

Build Log:\t${BUILD_URL}/console
Workspace:\t${BUILD_URL}/artifact/
            """, subject: "=?utf-8?Q?=E2=9A=A1__=5BEASE=5D_${JOB_NAME}_failed?=", to: "christian.pontesegger@web.de"
        }

        success {
            archiveArtifacts artifacts: 'releng/org.eclipse.ease.releng.p2/target/repository/, ease.module.doclet.jar, **/target/surefire-reports/*.xml', fingerprint: true            
        }
    }
}
