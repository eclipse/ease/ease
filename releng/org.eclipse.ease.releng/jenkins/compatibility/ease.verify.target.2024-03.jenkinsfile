pipeline {

	agent {
	    label 'migration'	    
	}

    options {
        timestamps()
        timeout(time: 60, unit: 'MINUTES')
    }

    environment {
		TARGET_FILE = "releng/org.eclipse.ease.releng.target/previous targets/2023-12.target"
        JAVA_VERSION = "openjdk-jdk17-latest"
        MAVEN_VERSION = "apache-maven-latest"
    }
    
    stages {

        stage('Checkout') {
            steps {
                checkout poll: false,
                scm: [$class: 'GitSCM',
                        branches: [[name: '*/main']],
                        doGenerateSubmoduleConfigurations: false,
                        extensions: [
                        	[$class: 'CleanCheckout'],
                        ],
                        submoduleCfg: [],
                        userRemoteConfigs: [[url: 'https://gitlab.eclipse.org/eclipse/ease/ease.git']]
                    ]
            }
        }

        stage('Test') {
            steps {
                sh """
                    cp "${TARGET_FILE}" "releng/org.eclipse.ease.releng.target/org.eclipse.ease.releng.target.target"
                """

	            withMaven(
	            	jdk: "${JAVA_VERSION}",
	            	maven: "${MAVEN_VERSION}",
	            	options: [
	            	    artifactsPublisher(disabled: true),
	            	    junitPublisher(disabled: true),
	            	    findbugsPublisher(disabled: true),
	            	    jgivenPublisher(disabled: true),
	            	    jacocoPublisher(disabled: true),
	            	    openTasksPublisher(disabled: true),
	            	    spotbugsPublisher(disabled: true)
	            	])
	            {
					sh """
					    export JENKINS_MAVEN_AGENT_DISABLED=true
					    mvn clean verify -f releng/org.eclipse.ease.releng/target-tests-pom.xml
					"""
                }

				junit '**/target/surefire-reports/*.xml'    	    
            }
        }
    }
    
    post {
        failure {
			archiveArtifacts artifacts: '**', fingerprint: false
			
            emailext subject: "=?utf-8?Q?=E2=9A=A1__=5BEASE=5D_${JOB_NAME}_failed?=", to: "christian.pontesegger@web.de", body: """
Build ${JOB_NAME} failed for TARGET = ${TARGET_FILE}

Build Log:\t${BUILD_URL}/console
Workspace:\t${BUILD_URL}/artifact/
            """
        }
    }
}
